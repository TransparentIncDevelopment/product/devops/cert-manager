.PHONY: install-dependencies

# See all Cert-Manager versions on this Github release page:
# https://github.com/cert-manager/cert-manager/releases
EXTERNAL_CERT_MANAGER_VERSION="1.9.1"

VERSION:=$(shell cat version.txt)

clean-output:
	rm -rf output

install-dependencies:
	mkdir -p output/k8s/cert-manager
	# This is the cert-manager k8s definitions
	wget https://github.com/cert-manager/cert-manager/releases/download/v${EXTERNAL_CERT_MANAGER_VERSION}/cert-manager.yaml -O output/k8s/cert-manager/cert-manager.yaml
	# This is the cert-manager CLI, used to verify cert-manager is ready after installation
	wget https://github.com/cert-manager/cert-manager/releases/download/v${EXTERNAL_CERT_MANAGER_VERSION}/cmctl-linux-amd64.tar.gz -O /tmp/cmctl-linux-amd64.tar.gz
	tar -xf /tmp/cmctl-linux-amd64.tar.gz -C output cmctl

copy-base:
	mkdir -p output/k8s/base
	cp -R k8s/base/* output/k8s/base

copy-http:
	mkdir -p output/k8s/issuer
	cp -R k8s/http/* output/k8s/issuer

copy-dns:
	mkdir -p output/k8s/issuer
	cp -R k8s/dns/* output/k8s/issuer

copy-deploy-script:
	cp scripts/deploy-cert-manager.py output

copy-cert-manager:
	cp -R k8s/cert-manager/* output/k8s/cert-manager

copy-cert-manager: install-dependencies copy-deploy-script copy-base

build-http: clean-output copy-cert-manager copy-http

build-dns: clean-output copy-cert-manager copy-dns
