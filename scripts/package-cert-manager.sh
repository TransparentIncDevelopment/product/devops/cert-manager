#!/usr/bin/env bash

set -e # Exit on error
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -eo pipefail

function helptext {
HELPTEXT="
    This script is intended to be run by automation. Given a zip filename, it packages the
    cert-manager for the given configuration option (http vs dns solver), excluding unused k8s definitions
    Arguments
	ZIP_FILENAME (Required) = Filename for resulting zip
	CONFIGURATION (Required) = One of \"http\" or \"dns\". This enum decides which configuration the
                             resulting cert-manager's ClusterIssuer is packaged with, and correspondingly,
                             how it will solve certificate challenges.
"
echo "$HELPTEXT"
}

function error {
    echo "$1"
    exit 1
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    error "$(helptext)

Missing arguments"
fi

# Verify args are set

ZIP_FILENAME=${1:?"$( error 'ZIP_FILENAME must be set' )"}
CONFIGURATION=${2:?"$( error 'CONFIGURATION must be set' )"}

# Build required variables
ZIP_FILE_DESTINATION="$(pwd)"/${ZIP_FILENAME}

# Get path to /cert-manager
SCRIPTS_DIR="$( dirname "${BASH_SOURCE[0]}" )"
CERT_MGR_REL_DIR="$( dirname "${SCRIPTS_DIR}" )"
CERT_MGR_ABS_DIR="$(realpath ${CERT_MGR_REL_DIR})"
OUTPUT_DIR="${CERT_MGR_ABS_DIR}/output"

case $CONFIGURATION in
     http)
       make build-http
          ;;
     dns)
       make build-dns
          ;;
     *)
          error "ERROR: CONFIGURATION \"$CONFIGURATION\" doesn't match a known configuration in the script."
          ;;
esac

# Print variables
echo "Path to cert-mgr dir: ${CERT_MGR_ABS_DIR}"
echo "Packaging for configuration: ${CONFIGURATION}"
echo "Given zip filename: ${ZIP_FILENAME}"

#Include only /k8s and /k8s-no-validate, exclude patch extensions
echo "Working directory: $(pwd)"
echo "Creating zip at: ${ZIP_FILE_DESTINATION}"

pushd ${OUTPUT_DIR}
#Include only /k8s and /k8s-no-validate, exclude patch extensions
zip -r  $ZIP_FILE_DESTINATION *

#zip -r $ZIP_FILE_DESTINATION . \
# -i "k8s/base/*" \
# -i "k8s/${CONFIGURATION}/*" \
# -i "cmctl" \
# -i "deploy-cert-manager.py"

popd
